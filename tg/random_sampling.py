import csv
import math
import random


class Sample:

    def __init__(self, data, number):
        self.data = data
        self.number = number

    def stat_data(self):
        data = self.data
        num_zero = 0
        num_not_zero = 0
        num_all = 0
        data_zero = []
        data_not_zero = []
        target_zero = []
        target_not_zero = []

        for datum in data:
            num_all += 1
            if datum[-1] is 0 or datum[-1] is '0':
                num_zero += 1
                target_zero.append(datum[-1])
                del datum[-1]
                data_zero.append(datum)
            else:
                num_not_zero += 1
                target_not_zero.append(datum[-1])
                del datum[-1]
                data_not_zero.append(datum)
        return num_zero, num_not_zero, num_all, data_zero, data_not_zero, target_zero, target_not_zero

    def sample_data(self):
        ratio = Sample.stat_data(self)

        # records
        records_zero = ratio[3]
        records_not_zero = ratio[4]
        target_zero = ratio[5]
        target_not_zero = ratio[6]

        # portfolio count
        num_portfolio_zero = math.floor((ratio[0] / ratio[2]) * self.number)
        num_portfolio_not_zero = self.number - num_portfolio_zero

        # sample indexes of portfolio
        indices_portfolio_zero = random.sample(range(len(records_zero)), num_portfolio_zero)
        indices_portfolio_not_zero = random.sample(range(len(records_not_zero)), num_portfolio_not_zero)

        # retrieve the portfolio
        portfolio_zero = [records_zero[i] for i in sorted(indices_portfolio_zero)]
        portfolio_not_zero = [records_not_zero[i] for i in sorted(indices_portfolio_not_zero)]
        portfolio_target_zero = [target_zero[i] for i in sorted(indices_portfolio_zero)]
        portfolio_target_not_zero = [target_not_zero[i] for i in sorted(indices_portfolio_not_zero)]

        return portfolio_zero + portfolio_not_zero, portfolio_target_zero + portfolio_target_not_zero


if __name__ == '__main__':
    with open("clean1-0.csv", "r") as lines:
        the_data = list(csv.reader(lines))
        sample = Sample(the_data, 10000)
        sample_data = sample.sample_data()
        print(len(sample_data))
