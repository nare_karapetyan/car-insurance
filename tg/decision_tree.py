import csv
import copy
import math
import random
import pandas
from sklearn import tree
from random_sampling import Sample
from sklearn.metrics import mean_squared_error


def do_regression():
    data_source = "clean1-0.csv"
    target = []

    with open(data_source, 'r') as lines:
        reader = csv.reader(lines)
        data = list(reader)
        del data[0]

        # original data has 63 features while data has 62
        original_data = copy.deepcopy(data)

    for attrs_row in data:
        target.append(attrs_row[-1])
        del attrs_row[-1]

    clf = tree.DecisionTreeRegressor()
    clf = clf.fit(data, target)

    sample = Sample(original_data, 10000)
    sample_data_pair = sample.sample_data()
    sample_data = sample_data_pair[0]
    sample_target = sample_data_pair[1]

    total_premium = 0
    for datum in sample_data:
        total_premium += float(datum[-1])

    prediction = clf.predict(sample_data)
    total_loss = 0
    for x in prediction:
        total_loss += x

    known_total_loss = 0
    for y in sample_target:
        known_total_loss += float(y)

    ratio = total_loss / total_premium
    actual_ratio = known_total_loss / total_premium

    f = open("out1.txt", "a")
    f.write("Loss: {0} \t Premium: {1} \t {2} \t {3} \n".format(total_loss, total_premium, ratio, actual_ratio))
    f.close()


def do_regression_without_putting_back():
    data_source = "clean1-0.csv"
    training_target = []

    data_frame = pandas.read_csv(data_source, low_memory=False)
    data = data_frame.values.tolist()

    row_number = len(data)
    training_number = math.floor(row_number * 0.8)

    training_data = []
    for _ in range(training_number):
        index = random.randrange(len(data))
        training_data.append(data.pop(index))

    testing_data = data

    for attrs_row in training_data:
        training_target.append(attrs_row[-1])
        del attrs_row[-1]

    clf = tree.DecisionTreeRegressor()
    clf = clf.fit(training_data, training_target)

    sample = Sample(testing_data, 10000)
    sample_data_pair = sample.sample_data()
    sample_data = sample_data_pair[0]
    sample_target = sample_data_pair[1]

    total_premium = 0
    for datum in sample_data:
        total_premium += float(datum[-1])

    total_loss = 0
    prediction = clf.predict(sample_data)
    for x in prediction:
        total_loss += x

    known_total_loss = 0
    for y in sample_target:
        known_total_loss += float(y)

    ratio = total_loss / total_premium
    actual_ratio = known_total_loss / total_premium

    file = open("decision_tree.csv", "a")
    file.write("{0}, {1}, {2}, {3}, {4}, {5}, {6}\n".format(
        known_total_loss, total_loss, abs(known_total_loss - total_loss),
        abs(known_total_loss - total_loss) / known_total_loss, total_premium,
        ratio, actual_ratio
    ))
    file.close()


if __name__ == '__main__':
    f = open("decision_tree.csv", "a")
    f.write("total_loss, prediction_loss, loss_difference, difference_ratio, total_premium, ratio, actual_ratio\n")
    f.close()
    for i in range(50):
        # do_regression()
        do_regression_without_putting_back()
