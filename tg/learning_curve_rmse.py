import math
import pandas
from matplotlib import pyplot as plot
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

infogain_list = [
    'Annual_Premium', 'SYS_Renewed', 'Vehicle_Collision_Coverage_Deductible',
    'EEA_Full_Coverage_Indicator',
    'Vehicle_Collision_Coverage_Indicator', 'EEA_Liability_Coverage_Only_Indicator',
    'Vehicle_Age_In_Years', 'Vehicle_Make_Year', 'Driver_Minimum_Age',
    'Vehicle_Symbol', 'Driver_Maximum_Age', 'Vehicle_Anti_Theft_Device',
    'Vehicle_Passive_Restraint', 'Vehicle_Miles_To_Work',
    'EEA_Packaged_Policy_Indicator',
    'Vehicle_New_Cost_Amount', 'Policy_Zip_Code_Garaging_Location',
    'EEA_Policy_Zip_Code_3', 'Vehicle_Usage', 'EEA_Policy_Tenure',
    'Policy_Installment_Term', 'SYS_New_Business',
    'Vehicle_Comprehensive_Coverage_Limit',
    'Vehicle_Bodily_Injury_Limit', 'EEA_Prior_Bodily_Injury_Limit',
    'Vehicle_Physical_Damage_Limit', 'Driver_Total_Young_Adult_Ages_24_29',
    'Policy_Billing_Code', 'Driver_Total_Female',
    'Driver_Total_Upper_Senior_Ages_70_plus',
    'Vehicle_Territory', 'Driver_Total_Male', 'Driver_Total_Married',
    'Vehicle_Performance', 'Policy_Method_Of_Payment',
    'Vehicle_Number_Of_Drivers_Assigned', 'Driver_Total_Adult_Ages_50_64',
    'Vehicle_Med_Pay_Limit', 'Vehicle_Driver_Points',
    'Driver_Total_Middle_Adult_Ages_40_49', 'Driver_Total_Related_To_Insured_Child',
    'Policy_Company', 'Driver_Total_Teenager_Age_15_19', 'EEA_Agency_Type',
    'Driver_Total_Related_To_Insured_Spouse', 'Policy_Reinstatement_Fee_Indicator',
    'Driver_Total_Senior_Ages_65_69', 'Driver_Total_Low_Middle_Adult_Ages_30_39',
    'EEA_Multi_Auto_Policies_Indicator', 'Vehicle_Youthful_Driver_Training_Code',
    'Vehicle_Safe_Driver_Discount_Indicator', 'Driver_Total_Single',
    'Vehicle_Comprehensive_Coverage_Indicator', 'Vehicle_Youthful_Driver_Indicator',
    'Driver_Total_Related_To_Insured_Self', 'Vehicle_Days_Per_Week_Driven',
    'Driver_Total_Licensed_In_State', 'Vehicle_Annual_Miles', 'Driver_Total',
    'Driver_Total_College_Ages_20_23', 'Vehicle_Youthful_Good_Student_Code']


def plot_rmse():
    data_frame = pandas.read_csv("data/clean2-1.csv", low_memory=False)
    train_data_frame, test_data_frame = train_test_split(data_frame, test_size=0.2)

    rmse_keys = []
    rmse_train_list = []
    rmse_predict_list = []
    feature_list_len = len(infogain_list)
    for i in range(11, feature_list_len+1, 5):
        feature_list = infogain_list[0:i]
        target_name = ['Loss_Amount']
        train_data_x = train_data_frame[feature_list].values
        train_data_y = train_data_frame[target_name].values
        test_data_x = test_data_frame[feature_list].values
        test_data_y = test_data_frame[target_name].values

        clf = tree.DecisionTreeRegressor()
        clf = clf.fit(train_data_x, train_data_y)

        rmse_keys.append(i)

        train_y_predict = clf.predict(train_data_x)
        mse_train = mean_squared_error(train_data_y, train_y_predict)
        rmse_train = math.sqrt(mse_train)
        rmse_train_list.append(rmse_train)

        test_y_predict = clf.predict(test_data_x)
        mse_predict = mean_squared_error(test_data_y, test_y_predict)
        rmse_predict = math.sqrt(mse_predict)
        rmse_predict_list.append(rmse_predict)

    plot_title = "RMSE Learning Curve - Without Outlier - Without 0 Claim"
    plot.figure()
    plot.grid()
    plot.title(plot_title)
    plot.xlabel("Feature Number")
    plot.ylabel("RMSE")
    plot.plot(rmse_keys, rmse_train_list)
    plot.plot(rmse_keys, rmse_predict_list)
    plot.legend()
    return plot


def plot_detailed_rmse():
    rmse_keys = []
    rmse_train_list = []
    rmse_predict_list = []
    for i in range(50000, 400001, 50000):
        data_frame = pandas.read_csv("data/clean2-0.csv", nrows=i, low_memory=False)
        train_data_frame, test_data_frame = train_test_split(data_frame, test_size=0.2)

        feature_list = infogain_list[0:36]
        target_name = ['Loss_Amount']
        train_data_x = train_data_frame[feature_list].values
        train_data_y = train_data_frame[target_name].values
        test_data_x = test_data_frame[feature_list].values
        test_data_y = test_data_frame[target_name].values

        clf = tree.DecisionTreeRegressor()
        clf = clf.fit(train_data_x, train_data_y)

        rmse_keys.append(i)

        train_y_predict = clf.predict(train_data_x)
        mse_train = mean_squared_error(train_data_y, train_y_predict)
        rmse_train = math.sqrt(mse_train)
        rmse_train_list.append(rmse_train)

        test_y_predict = clf.predict(test_data_x)
        mse_predict = mean_squared_error(test_data_y, test_y_predict)
        rmse_predict = math.sqrt(mse_predict)
        rmse_predict_list.append(rmse_predict)

    plot_title = "Training Example - RMSE Learning Curve - Without Outlier - With 0 Claim"
    plot.grid()
    plot.title(plot_title)
    plot.xlabel("Training Example Number")
    plot.ylabel("RMSE")
    plot.plot(rmse_keys, rmse_train_list)
    plot.plot(rmse_keys, rmse_predict_list)
    return plot


# plotting = plot_rmse()
plotting = plot_detailed_rmse()
plotting.show()
